import { Injectable } from '@angular/core';
import { RequestOptions } from "@angular/http";
import { HttpService } from "./http.service";

@Injectable()
export class AuthService {



    getUserToken() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return currentUser.token;
    }
    getUserData() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return currentUser;
    }
    getUser() {
        let currentUser = JSON.parse(localStorage.getItem('user'));
        return currentUser;
    }
    getUrl() {
        return 'http://apiboletas.planificacion.gob.bo/api/';
    }
    getUrlUploads() {
        return 'http://apiboletas.planificacion.gob.bo/api/boletas/upload/';
    }


}
