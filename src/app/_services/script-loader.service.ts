import { Injectable, OnInit } from "@angular/core";
import * as $ from 'jquery';
import { HttpService } from "./http.service";
import { Subject } from "rxjs/Subject";

declare let document: any;

interface Script {
    src: string;
    loaded: boolean;
}

@Injectable()
export class ScriptLoaderService {
    private _scripts: Script[] = [];
    private tag: any;
    por_vencer: any = [];
    profile: any = [];
    private notifSource = new Subject<any>();
    public notif$ = this.notifSource.asObservable();
    private profileChange = new Subject<any>();
    public profile$ = this.profileChange.asObservable();
    constructor(private _boleta: HttpService) {
        //console.log('*CONSTRUCTOR');
    }
    getPorVencer() {
        //console.log('*POR VENCER');
        this._boleta.dashboard().subscribe(data => {
            this.por_vencer = data[2];
            //console.log('POR VENCER', this.por_vencer);
            this.notifSource.next(this.por_vencer);
        });
    }
    getProfile() {
        this._boleta.getUser().subscribe(data => {
            localStorage.setItem('user', JSON.stringify(data.data[0]));
            this.profile=data;
            this.profileChange.next(this.profile.data);
        });
    }

    load(tag, ...scripts: string[]) {
        //console.log('*LOAD');
        this.getPorVencer();
        this.getProfile();
        this.tag = tag;
        scripts.forEach((script: string) => this._scripts[script] = { src: script, loaded: false });

        let promises: any[] = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    loadScript(src: string) {
        //console.log('*LOADSCRIPT');
        return new Promise((resolve, reject) => {

            //resolve if already loaded
            if (this._scripts[src].loaded) {
                resolve({ script: src, loaded: true, status: 'Already Loaded' });
            }
            else {
                //load script
                let script = $('<script/>')
                    .attr('type', 'text/javascript')
                    .attr('src', this._scripts[src].src);

                $(this.tag).append(script);
                resolve({ script: src, loaded: true, status: 'Loaded' });
            }
        });
    }
    getUser() {
        let currentUser = JSON.parse(localStorage.getItem('user'));
        return currentUser;
    }


}