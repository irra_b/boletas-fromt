import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from "@angular/http";
import { AuthService } from "./auth.service";

@Injectable()
export class HttpService {

    constructor(private http: Http, private service: AuthService) { }
    getBoletas() {
        return this.http.get(this.service.getUrl() + 'boletas/', this.jwt()).map((response: Response) => response.json());
    }
    getParams() {
        return this.http.get(this.service.getUrl() + 'boletas/params/', this.jwt()).map((response: Response) => response.json());
    }
    getUser() {
        return this.http.get(this.service.getUrl() + 'boletas/profile/', this.jwt()).map((response: Response) => response.json());
    }
    getBoleta(id) {
        return this.http.get(this.service.getUrl() + 'boletas/' + id, this.jwt()).map((response: Response) => response.json());
    }
    addBoleta(data) {
        return this.http.post(this.service.getUrl() + 'boletas/', data, this.jwt()).map((response: Response) => response.json());
    }
    updateBoleta(data) {
        return this.http.put(this.service.getUrl() + 'boletas/', data, this.jwt()).map((response: Response) => response.json());
    }
    updateImg(data) {
        return this.http.put(this.service.getUrl() + 'boletas/profile/', data, this.jwt()).map((response: Response) => response.json());
    }
    deleteBoleta(data) {
        return this.http.delete(this.service.getUrl() + 'boletas/' + data, this.jwt()).map((response: Response) => response.json());
    }
    addEntidad(data) {
        return this.http.post(this.service.getUrl() + 'boletas/entidad/', data, this.jwt()).map((response: Response) => response.json());
    }
    report(data) {
        return this.http.post(this.service.getUrl() + 'boletas/report/', data, this.jwt()).map((response: Response) => response.json());
    }
    isFree(id) {
        return this.http.get(this.service.getUrl() + 'boletas/isfree/' + id, this.jwt()).map((response: Response) => response.json());
    }
    deleteEntidad(data) {
        return this.http.delete(this.service.getUrl() + 'boletas/entidad/' + data, this.jwt()).map((response: Response) => response.json());
    }
    cumplimiento(data) {
        return this.http.post(this.service.getUrl() + 'boletas/cumplimiento/', data, this.jwt()).map((response: Response) => response.json());
    }
    ejecutado(data) {
        return this.http.post(this.service.getUrl() + 'boletas/ejecutado/', data, this.jwt()).map((response: Response) => response.json());
    }
    dashboard() {
        return this.http.get(this.service.getUrl() + 'boletas/dashboard/', this.jwt()).map((response: Response) => response.json());
    }
    verify(pass,id) {
        return this.http.get(this.service.getUrl() + 'auth/verify/'+pass+'/'+id, this.jwt()).map((response: Response) => response.json());
    }
    updatePass(id,data) {
        return this.http.put(this.service.getUrl() + 'auth/pass/'+id, data, this.jwt()).map((response: Response) => response.json());
    }
    setEditable(data) {
        return this.http.put(this.service.getUrl() + 'boletas/editable/', data, this.jwt()).map((response: Response) => response.json());
    }
    getBoletasByColor(param) {
        return this.http.get(this.service.getUrl() + 'boletas/color/' + param, this.jwt()).map((response: Response) => response.json());
    }
    recoverPassword(data) {
        return this.http.post(this.service.getUrl() + 'auth/recover-password/',data, this.jwt()).map((response: Response) => response.json());
    }
    //******************************//
    getToken() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return currentUser.token;
    };
    getUploadsUrl() {
        return this.service.getUrlUploads();
    };
    getUrl() {
        return this.service.getUrl();
    };
    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        console.log(currentUser);
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }



}
