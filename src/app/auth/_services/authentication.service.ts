import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/operator/map";
import { AuthService } from "../../_services/auth.service";

@Injectable()
export class AuthenticationService {
    constructor(private http: Http, private service: AuthService) {
    }

    login(email: string, password: string) {
        ///api/authenticate
        return this.http.post(this.service.getUrl() + 'auth/', JSON.stringify({ email: email, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                //console.log('ENTRO');
                //console.log(response.json());
                let user = response.json();
                //console.log(user);
                if (user && user.data.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    //console.log('LOGIN');
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                } else {
                    throw "Usuario y/o contraseña invalidos.";
                }
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}