import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpService} from "../../_services/http.service";
import notify from "devextreme/ui/notify";

@Component({
  selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
  templateUrl:'./recover.component.html',
  styles: []
})
export class RecoverComponent implements OnInit {

  model:any={password:'',password2:'',token:''};
    isVisible:boolean=false;
    message:string;
    type:string;
    sw:boolean=false;
  constructor(private router: Router,private route: ActivatedRoute,private _boleta:HttpService) {

  }

  ngOnInit() {
      this.route.queryParams.subscribe(params => {
          if(params.t!=undefined){
              this.model.token=params.t;
          }
      });
  }
  onHidden(e){
    if(this.sw){
        this.router.navigate(['/']);
    }
  }
  enviar(){
    console.log(this.model);
    if(this.model.password==this.model.password2){
        if(this.model.password.length>5){
            this._boleta.recoverPassword(this.model).subscribe(data=>{
                console.log(data);
                this.isVisible=true;
                this.type='success';
                this.message='Contraseña cambiada correctamente...';
                this.sw=true;
            },err=>{
                let msg=JSON.parse(err._body);
                notify(msg.status, 'error', 1000);
                this.isVisible=true;
                this.type='error';
                this.message=msg.status;
                console.log(err);
            });
        }else{
            this.isVisible=true;
            this.type='error';
            this.message='La contraseña deben ser minimamente de 6 caracteres.';
        }

    }else{
        this.isVisible=true;
        this.type='error';
        this.message='Las contraseñas deben ser iguales.';
    }


  }

}
