import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { Http, Response, Headers } from "@angular/http";
import { AuthService } from "../../../_services/auth.service";

@Component({
    selector: 'app-main',
    templateUrl: "./main.component.html",
    styles: []
})
export class MainComponent implements OnInit {
    http: Http;
    empresas: any = [];
    rubros: any = [];
    tipos: any = [];
    departamentos: any = [];
    enlaces: any = [];
    estados: any = [];
    headers: Headers;
    opened: boolean = false;
    estado_selected: number;
    empresa_selected: number;
    showProgress: boolean = false;
    constructor(http: Http, private service: AuthService) {
        this.http = http;
        this.getEmpresas();
        var headers = new Headers();
        headers.append('authorization', this.service.getUserToken());
        this.http.get(this.service.getUrl() + 'params/', { headers }).subscribe((res: Response) => {
            this.rubros = res.json()[0];
            this.tipos = res.json()[1];
            this.departamentos = res.json()[2];
            this.enlaces = res.json()[3];
            this.estados = res.json()[4];
        });
    }
    getEmpresas() {
        var headers = new Headers();
        headers.append('authorization', this.service.getUserToken());
        this.http.get(this.service.getUrl() + 'empresas-data/', { headers }).subscribe((res: Response) => {
            this.empresas = res.json();
        });
    }
    ngOnInit() { }
    cambiar(e) {
        //console.log(e);
        this.empresa_selected = e.id;
        this.estado_selected = e.estado;
        this.opened = true;
    }
    guardar() {
        this.showProgress = true;
        var headers = new Headers();
        headers.append('authorization', this.service.getUserToken());
        this.http.put(this.service.getUrl() + 'empresa-estado/', { empresa: this.empresa_selected, estado: this.estado_selected }, { headers }).subscribe((res: Response) => {
            //console.log(res.json());
            this.getEmpresas();
            this.cancel();
            this.showProgress = false;
        });
        //console.log(this.empresa_selected);
    }

    cellVal(rowData) {
        return rowData.estado;
    }
    cellVal2(rowData) {
        return '<a href="#">sss</a>';
    }
    open() {

        this.opened = !this.opened;
    }

    cancel() {
        this.opened = false;
    }

}
