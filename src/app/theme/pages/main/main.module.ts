import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from "./main.component";
import { DefaultComponent } from "../default/default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../layouts/layout.module";
import { DxButtonModule, DxDataGridModule, DxTemplateModule } from "devextreme-angular";
import { NglModule } from "ng-lightning";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": MainComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, DxButtonModule, DxDataGridModule, DxTemplateModule, DxButtonModule, NglModule.forRoot()
    ], exports: [
        RouterModule
    ], declarations: [
        MainComponent
    ]
})
export class MainModule { }
