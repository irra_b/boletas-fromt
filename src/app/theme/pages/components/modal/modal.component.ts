import {
    Attribute, Component, OnInit, AfterViewInit, ViewEncapsulation, Output, EventEmitter,
    ViewChild
} from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder } from "@angular/forms";
import { AuthService } from "../../../../_services/auth.service";
@Component({
    selector: 'app-modal',
    templateUrl: "./modal.component.html"
})
export class ModalComponent implements OnInit {

    x: any;
    id: any;
    title: string;
    content: string;
    http: Http;
    data: any = { nombre: '', nit: '', tipo: {}, rubro: {}, departamento: {}, direccion: '', registrada: false, nombre_contacto: '', telefono: '', email: '', enlace: {}, medio: '', obs: '' };
    marker: any;
    rubro: any;
    rubros: any = [];
    tipos: any = [];
    enlaces: any = [];
    departamentos: any = [];
    estados: any = [];
    showCargando: boolean = false;
    edit: boolean = false;
    @Output() endSubmit: EventEmitter<string> = new EventEmitter();
    opened: boolean = false;
    form: any;
    showProgress: boolean = false;
    text: string;
    contactos: any = [];
    @Output() onOpenPopup = new EventEmitter();
    constructor(@Attribute('name') name, http: Http, private _script: ScriptLoaderService, private service: AuthService) {
        this.id = name;
        this.http = http;
        //console.log('constructor');
        var headers: any = new Headers();
        headers.append('authorization', service.getUserToken());
        this.http.get(service.getUrl() + 'params/', { headers }).subscribe((res: Response) => {
            this.rubros = res.json()[0];
            this.tipos = res.json()[1];
            this.departamentos = res.json()[2];
            this.enlaces = res.json()[3];
            this.estados = res.json()[4];
        });
    }
    addContacto() {
        this.contactos.push({ id: '', representante: '', numero: '', email: '' });
    }
    open() {
        this.text = 'Confirme';
        this.opened = !this.opened;
    }

    cancel() {
        this.opened = false;
    }
    lookup = (query: string, source = this.rubros): string[] => {
        //console.log(query);
        if (!query) {
            return null;
        }
        var r = source.filter((d: any) => d.nombre.toLowerCase().indexOf(query.toLowerCase()) > -1);
        return source.filter((d: any) => d.nombre.toLowerCase().indexOf(query.toLowerCase()) > -1);
    }

    ngOnInit() {
        //console.log('init');

    }
    setTitle(title: string) {
        this.title = title;
    }
    setContent(content: string) {
        this.content = content;
    }
    show(marker: any) {
        //this.marker = marker;
        this.x = $('#modal');
        this.x.modal('toggle');
    }
    showEdit(marker: any) {
        this.showCargando = true;
        this.edit = true;
        this.marker = marker;
        this.x = $('#modal');
        //console.log(marker.id);
        this.x.modal('toggle');
        var headers: any = new Headers();

        headers.append('authorization', this.service.getUserToken());
        this.http.get(this.service.getUrl() + 'empresa/' + marker.id, { headers }).subscribe((res: Response) => {
            this.data = res.json();
            this.tipos.find(function(currentValue) {
                if (parseInt(this.tipo) == currentValue.id) {
                    this.tipo = currentValue;
                }
            }, this.data);
            this.departamentos.find(function(currentValue) {
                if (parseInt(this.departamento) == currentValue.id) {
                    this.departamento = currentValue;
                }
            }, this.data);
            this.enlaces.find(function(currentValue) {
                if (parseInt(this.enlace) == currentValue.id) {
                    this.enlace = currentValue;
                }
            }, this.data);
            this.rubro = this.rubros.find(function(currentValue) {
                if (parseInt(this.rubro) == currentValue.id) {
                    return currentValue;
                }
            }, this.data);
            this.estados.find(function(currentValue) {
                if (parseInt(this.estado) == currentValue.id) {
                    this.estado = currentValue;
                }
            }, this.data);
            this.showCargando = false;
            //console.log(this.data);
        });
    }
    close() {
        this.x = $('#modal');
        this.x.modal('toggle');
    }

    loadSc() {
        //console.log('after');

    }
    onSubmit(form: any): void {
        console.log('onsubmit');
        this.form = form;
        this.open();
    }
    sendData() {
        this.text = 'Enviando... ';
        this.showProgress = true;
        var headers: any = new Headers();
        headers.append('authorization', this.service.getUserToken());
        this.data.rubro = this.rubro;
        //console.log(this.data);
        if (this.edit) {
            this.http.put(this.service.getUrl() + 'empresa/', this.data, { headers }).subscribe((res: Response) => {
                //console.log(res.json());
                this.showProgress = false;
                this.opened = false;
                this.close();
                this.edit = false;
                this.form.reset();
                this.endSubmit.emit('send');
            });
        } else {
            this.data.marker = this.marker;
            this.http.post(this.service.getUrl() + 'empresa/', this.data, { headers }).subscribe((res: Response) => {
                //console.log(res.json());
                this.showProgress = false;
                this.opened = false;
                this.close();
                this.edit = false;
                this.form.reset();
                this.endSubmit.emit('send');
            });
        }
    }
    showContactosModal() {
        //console.log('ENTRO A MODAL');
        this.onOpenPopup.emit();
    }
}
