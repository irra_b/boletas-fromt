import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpService } from "../../../../_services/http.service";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { PercentPipe } from "@angular/common";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./index.component.html",
    styles: []

})
export class IndexComponent implements OnInit, AfterViewInit {

    estados: any;
    color: any = [{ cantidad: '', total: '' }, { cantidad: '', total: '' }, { cantidad: '', total: '' }, { cantidad: '', total: '' }];
    total: number;
    pipe: any = new PercentPipe("en-US");
    por_vencer: any = [];
    customizeTooltip: any;
    populationByRegions: any = [];
    constructor(private _boleta: HttpService, private router: Router) {


    }
    ngAfterViewInit() {

    }
    ngOnInit() {

        //console.log('CALL');
        this.call();


    }
    call() {
        let s: number = 0;
        this._boleta.dashboard().subscribe(data => {
            this.estados = data[0];
            this.color = data[1];
            this.por_vencer = data[2];
            this.color.forEach((currentValue, index, array) => {
                s += Number(currentValue.total);
            });
            this.color.forEach((currentValue, index, array) => {
                currentValue.p = ((Number(currentValue.total) * 100) / s).toFixed();
                currentValue.color = 'red';
            });
            this.total = s;
            //console.log('ESTADOS', this.estados);
            this.estados.forEach((currentValue, index, array) => {
                this.populationByRegions.push({ title: currentValue.estado, val: Number(currentValue.total) });
            });

        });

    }

    customizeText(arg) {
        return arg.valueText + " (" + arg.percentText + ")";
    }
    getBoletas(i) {
        //console.log(i);
        this.por_vencer = [];
        this._boleta.getBoletasByColor(i).subscribe(data => {
            //console.log(data);
            if (data.status == 200) {
                this.por_vencer = data.data;
                //console.log(this.por_vencer);
            }
        });
    }


}