import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { ModalComponent } from "../../components/modal/modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NglModule } from "ng-lightning";
import { HttpService } from "../../../../_services/http.service";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { DxPieChartModule } from "devextreme-angular";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": IndexComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NglModule.forRoot(),
        DxPieChartModule



    ], exports: [
        RouterModule
    ], declarations: [
        IndexComponent, ModalComponent
    ],
    providers: [HttpService]
})
export class IndexModule {



}