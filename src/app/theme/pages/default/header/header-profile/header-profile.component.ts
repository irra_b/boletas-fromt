import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { AuthService } from "../../../../../_services/auth.service";
import { Http, Headers, Response } from "@angular/http";
import {HttpService} from "../../../../../_services/http.service";
import {ScriptLoaderService} from "../../../../../_services/script-loader.service";
import {Subscription} from "rxjs/Rx";


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./header-profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderProfileComponent implements OnInit {


    user: any = {};
    showAlert: boolean = false;
    severity: string;
    aler_message: string;
    isInvalid: boolean = true;
    data: any = { actual: { error: true, value: '' }, nueva: '', confirmar: '', error: true };
    popupVisible = false;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    idUser:any;
    private profile: Subscription = null;
    constructor(private service: AuthService, private _boleta: HttpService,private _s:ScriptLoaderService) {
        this.user=service.getUser();
        this.idUser=service.getUserData();
        if(this.user.img==undefined){
            this.user.img='./assets/images/avatar1.jpg';
        }
        this.croppedImage=this.user.img;
    }

    ngOnInit() {
        this.profile=this._s.profile$.subscribe((r)=>{
            //console.log('PROFILE SUBSCRIPTION profile',r);
            this.user=r[0];
        });
    }
    verify(e) {
        if(e.type=='keypress'){
            if(e.key=='Enter'){
                if(this.data.actual.value.length>5){
                    this._boleta.verify(this.data.actual.value, this.idUser.id).subscribe((res) => {
                            if (res.status == 200) {
                                this.data.actual.error = false;
                            } else {
                                this.data.actual.error = true;
                            }
                            //console.log(res);
                        },
                        err => {
                            this.data.actual.error = true;
                        });
                }
            }
        }
    }
    verifyOnBlur(e) {
                if(this.data.actual.value.length>5){
                    this._boleta.verify(this.data.actual.value, this.idUser.id).subscribe((res) => {
                            if (res.status == 200) {
                                this.data.actual.error = false;
                            } else {
                                this.data.actual.error = true;
                            }
                            //console.log(res);
                        },
                        err => {
                            this.data.actual.error = true;
                        });
                }

    }
    verify2() {
        if (this.data.nueva == this.data.confirmar) {
            this.data.error = false;
        } else {
            this.data.error = true;
        }
    }
    onSubmit() {
        this._boleta.updatePass(this.idUser.id, this.data).subscribe((res) => {
            if (res.status == 200) {
                this.showAlert = true;
                this.severity = 'success';
                this.aler_message = 'La contraseña fue cambiada correctamente...';

            } else {
                this.showAlert = true;
                this.severity = 'error';
                this.aler_message = res.json().msg;
            }
        });
    }
    show() {
        this.showAlert = true;
    }

    onClose(reason: string) {
        //console.log(`Alert closed by ${reason}`);
        this.showAlert = false;
    }
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(image: string) {
        this.croppedImage = image;
        //console.log(this.croppedImage);
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }
    update(){
        this._boleta.updateImg({img:this.croppedImage}).subscribe((r)=>{
            if(r.status==200){
                this.popupVisible=false;
                this._s.getProfile();
            }

        })
    }

}