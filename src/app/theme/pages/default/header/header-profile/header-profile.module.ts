import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HeaderProfileComponent } from './header-profile.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NglModule } from "ng-lightning";
import { DxButtonModule, DxPopupModule, DxTemplateModule } from "devextreme-angular";
import { ImageCropperModule } from "ngx-image-cropper";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": HeaderProfileComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NglModule.forRoot(),
        DxPopupModule,
        DxButtonModule,
        DxTemplateModule,
        ImageCropperModule

    ], exports: [
        RouterModule
    ], declarations: [
        HeaderProfileComponent
    ]
})
export class HeaderProfileModule {



}