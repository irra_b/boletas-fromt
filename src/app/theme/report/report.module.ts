import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from "../pages/default/default.component";
import { RouterModule, Routes } from "@angular/router";
import { ReportComponent } from "./report.component";
import { LayoutModule } from "../layouts/layout.module";
import {
    DxButtonModule, DxDropDownBoxModule, DxFormModule, DxListModule, DxPopupModule,
    DxTextAreaModule
} from "devextreme-angular";
import { NglModule } from "ng-lightning";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpService } from "../../_services/http.service";
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ReportComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, DxButtonModule, NglModule.forRoot(), FormsModule,
        ReactiveFormsModule, DxPopupModule,
        DxTextAreaModule,
        DxFormModule, DxDropDownBoxModule,
        DxListModule
    ], exports: [
        RouterModule
    ], declarations: [
        ReportComponent
    ],
    providers: [HttpService]
})
export class ReportModule { }
