import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpService } from "../../_services/http.service";

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: 'report.component.html',
    styles: []
})
export class ReportComponent implements OnInit {

    items = [];
    pick: any = [];
    monedas: any = [{ nombre: 'Bs' }, { nombre: '$us' }];
    moneda: any;
    open: boolean;
    constructor(private _boleta: HttpService, private router: Router) {
        this._boleta.getParams().subscribe(data => {
            //console.log('DATA', data);
            this.items = data.estados;
            this.items.splice(3, 1);
        });
    }

    ngOnInit() {
    }
    report() {
        //console.log(this.moneda);
        let d: any = { pick: this.pick, moneda: this.moneda.nombre };
        this._boleta.report(d).subscribe(data => {
            //console.log('DATA', data);
            this.getPdf(data);
        });
    }
    getPdf(data: any) {
        let obj: any;
        obj = data;
        var binary = atob(obj.data);
        var buffer = new ArrayBuffer(binary.length);
        var bytes = new Uint8Array(buffer);
        for (var i = 0; i < buffer.byteLength; i++) {
            bytes[i] = binary.charCodeAt(i) & 0xFF;
        }

        let myblob = new Blob([buffer], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(myblob);
        //console.log(fileURL);
        window.open(fileURL);
    }


}
