import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from "../../_services/http.service";
import { Router } from "@angular/router";
import { DxFormComponent } from "devextreme-angular";
import { AuthService } from "../../_services/auth.service";
@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './boleta.component.html',
    styles: []
})
export class BoletaComponent implements OnInit {

    alert: any = [{ severity: '', message: '', show: false }];
    defaultVisible = false;
    boletas: any = [];
    boleta: any = {};
    popupEditableVisible: boolean;
    titulo: string;
    bancos: any = [];
    tipos: any = [];
    beneficiarios: any = [];
    estados: any = [];
    popupVisible: boolean = false;
    itemSelected: any;
    num_boleta: string;
    actionButtons: any = { ej: { disabled: true }, re: { disabled: true }, cu: { disabled: true } };
    popupVisible2: boolean = false;
    employee: any = { observaciones: '', hr: '', fecha: '', id: '', archivos: [], boleta: '' };
    value: any[] = [];
    headers: any = {};
    showFileMaxMessage: boolean = false;
    selectedItem: any;
    uploadsUrl: string;
    uploadUrl: string;
    url: string;
    user: any;
    popupViewVisible: boolean = false;
    path_imagen: string;
    @ViewChild(DxFormComponent) dataGrid: DxFormComponent;
    @ViewChild('fileUploader') fileUploader: any;
    constructor(private _boleta: HttpService, private router: Router, private _user: AuthService) {
        this.headers.authorization = this._boleta.getToken();
        this.url = this._boleta.getUrl().replace('api/', '');
        this.uploadsUrl = this._boleta.getUploadsUrl().replace('api/boletas/upload/','');
        this.uploadUrl = this._boleta.getUploadsUrl();
        this.user = _user.getUserData();
        console.log('UPLOADS', this.uploadsUrl);
    }

    ngOnInit() {
        this.llenar();
    }
    llenar() {
        this._boleta.getBoletas().subscribe(data => {
            this.boletas = data;
        });
        this._boleta.getParams().subscribe(data => {
            this.bancos = data.bancos;
            this.tipos = data.tipos;
            this.beneficiarios = data.beneficiarios;
            this.estados = data.estados;
        });
    }
    nuevo() {
        console.log('nuevo');
        this.router.navigateByUrl('/registro');
    }
    editar(e) {
        console.log(e.id);
        this.router.navigate(['/registro'], { queryParams: { id: e.id } });
    }
    eliminar() {

        this._boleta.deleteBoleta(this.itemSelected.id).subscribe(data => {
            if (data.status == 200) {
                this.popupVisible = false;
                this.llenar();
            }
        });

    }
    confirmar(e) {
        console.log(e.id);
        this.num_boleta = e.codigo;
        this.itemSelected = e;
        this.popupVisible = true;
    }
    cambiar(e) {
        console.log(e.id);
    }
    mostrarPopup() {
        this.showFileMaxMessage = false;
        this.popupVisible2 = true;
        this.titulo = 'Ejecución de Boleta';
    }
    cumplimiento() {
        this.popupVisible2 = true;
        this.titulo = 'Cumplimiento de Boleta';
    }

    onSelectionChanged(e) {
        this.selectedItem = e.selectedRowsData[0].id;
        this.actionButtons.ej.disabled = true;
        this.actionButtons.re.disabled = true;
        this.actionButtons.cu.disabled = true;
        this.employee = { observaciones: '', hr: '', fecha: new Date(), id: '', archivos: [], boleta: '' };
        if (e.selectedRowsData[0].invert == 1) {
            this.actionButtons.ej.disabled = false;
            this.actionButtons.re.disabled = false;
            this.actionButtons.cu.disabled = false;
        }
        if (e.selectedRowsData[0].invert == 0) {
            this.actionButtons.ej.disabled = true;
            this.actionButtons.re.disabled = false;
            this.actionButtons.cu.disabled = false;
        }
        if (e.selectedRowsData[0].estado == 27) {
            var f = new Date(e.selectedRowsData[0].ejecutado[0].fecha);
            this.actionButtons.ej.disabled = false;
            this.actionButtons.re.disabled = true;
            this.actionButtons.cu.disabled = true;
            this.employee.id = e.selectedRowsData[0].ejecutado[0].id;
            this.employee.boleta = e.selectedRowsData[0].ejecutado[0].boleta;
            this.employee.fecha = f;
            this.employee.hr = e.selectedRowsData[0].ejecutado[0].hr;
            this.employee.observaciones = e.selectedRowsData[0].ejecutado[0].obs;
            this.employee.archivos = e.selectedRowsData[0].ejecutado[0].archivos;
            console.log(this.employee);
        }
        if (e.selectedRowsData[0].estado == 28) {
            var f = new Date(e.selectedRowsData[0].cumplimiento[0].fecha);
            this.actionButtons.ej.disabled = true;
            this.actionButtons.re.disabled = true;
            this.actionButtons.cu.disabled = false;
            this.employee.id = e.selectedRowsData[0].cumplimiento[0].id;
            this.employee.boleta = e.selectedRowsData[0].cumplimiento[0].boleta;
            this.employee.fecha = f;
            this.employee.hr = e.selectedRowsData[0].cumplimiento[0].hr;
            this.employee.observaciones = e.selectedRowsData[0].cumplimiento[0].obs;
            this.employee.archivos = e.selectedRowsData[0].cumplimiento[0].archivos;
            console.log(this.employee);
        }
    }
    add(f) {
        this.employee.boleta = this.selectedItem;
        console.log('DATA', this.employee);
        if (this.titulo == 'Cumplimiento de Boleta') {
            this._boleta.cumplimiento(this.employee).subscribe((res: Response) => {
                if (res.status == 200) {
                    this.popupVisible2 = false;
                    this.llenar();
                } else {
                }
            });
        } else {
            this._boleta.ejecutado(this.employee).subscribe((res: Response) => {
                if (res.status == 200) {
                    this.popupVisible2 = false;
                    this.llenar();
                } else {
                }
            });
        }


    }
    hidePopup() {
        this.dataGrid.instance.resetValues();
        console.log(this.fileUploader.instance.reset());
    }
    progress(e) {
        console.log('PROGRESS', e);
    }

    upload(f, start, end) {
        var reader = new FileReader();

        // If we use onloadend, we need to check the readyState.
        reader.onloadend = (evt) => {
            console.log('DONE', reader.result);

        };
        var blob = f.slice(start, end);
        reader.readAsBinaryString(blob);
    }
    uploaded(e) {
        console.log('END', JSON.parse(e.request.response));
        this.employee.archivos.push(JSON.parse(e.request.response));
    }
    remove(index) {
        this.employee.archivos.splice(index, 1);
    }
    uploadError(e) {
        console.log('ERR', e);
    }
    changed(e) {
        console.log('CHN', e.value[0].size);
        if (e.value[0].size > 20971520) {
            this.fileUploader.instance.reset();
            this.showFileMaxMessage = true;
        } else {
            this.showFileMaxMessage = false;
        }

    }
    renovar() {
        this.router.navigate(['/registro'], { queryParams: { id: this.selectedItem, renov: 'true' } });
    }
    showFile(file) {
        console.log(file);
        window.open(this.url + file.uri);
    }
    editable(e) {
        this.num_boleta = e.codigo;
        this.itemSelected = e;
        this.popupEditableVisible = true;
    }
    setEditable() {
        this._boleta.setEditable(this.itemSelected.id).subscribe(data => {
            if (data.status == 200) {
                this.alert.severity = 'success';
                this.alert.message = 'Se habilito la edición de la boleta:' + this.num_boleta;
                this.alert.show = true;
                this.llenar();
            } else {
                this.alert.severity = 'error';
                this.alert.message = data.msg;
                this.alert.show = true;


            }
            this.popupEditableVisible = false;
        });
    }
    onClose(reason: string) {
        this.alert.show = false;
    }
    ver(data) {
        this.boleta=data;
        console.log(this.boleta,this.uploadsUrl);
        this.popupViewVisible = true;
    }
    mostrar(uri){
        console.log(uri);
        window.open(this.uploadsUrl+uri,'_blank');
    }
}
