import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from "../pages/default/default.component";
import { RouterModule, Routes } from "@angular/router";
import { BoletaComponent } from "./boleta.component";
import { LayoutModule } from "../layouts/layout.module";
import {
    DxButtonModule, DxDataGridModule, DxFileUploaderModule, DxFormModule, DxPopoverModule, DxPopupModule,
    DxSelectBoxModule, DxTemplateModule,
    DxTextAreaModule,
    DxTooltipModule, DxValidationSummaryModule, DxValidatorModule
} from "devextreme-angular";
import { NglModule } from "ng-lightning";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpService } from "../../_services/http.service";
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BoletaComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, DxButtonModule, DxDataGridModule, DxTemplateModule, DxButtonModule, NglModule.forRoot(), FormsModule,
        ReactiveFormsModule, DxPopupModule, DxTemplateModule, DxTooltipModule, DxPopoverModule, DxSelectBoxModule, DxValidatorModule,
        DxValidationSummaryModule, DxFileUploaderModule,
        DxTextAreaModule,
        DxFormModule
    ], exports: [
        RouterModule
    ], declarations: [
        BoletaComponent
    ],
    providers: [HttpService]
})
export class BoletaModule { }
