import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";

const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path": "main",
                "loadChildren": ".\/pages\/main\/main.module#MainModule"
            },
            {
                "path": "detalle/:id",
                "loadChildren": ".\/components\/detalle\/detalle.module#DetalleModule"
            },
            {
                "path": "ejec/:id",
                "loadChildren": ".\/forms\/ejecucion\/ejecucion.module#EjecucionModule"
            },
            {
                "path": "boletas",
                "loadChildren": ".\/boleta\/boleta.module#BoletaModule"
            },
            {
                "path": "reporte",
                "loadChildren": ".\/report\/report.module#ReportModule"
            },

            {
                "path": "registro",
                "loadChildren": ".\/forms\/registro\/registro.module#RegistroModule"
            },
            {
                "path": "registro/:id",
                "loadChildren": ".\/forms\/registro\/registro.module#RegistroModule"
            },
            {
                "path": "index",
                "loadChildren": ".\/pages\/default\/index\/index.module#IndexModule"
            },
            {
                "path": "header\/actions",
                "loadChildren": ".\/pages\/default\/header\/header-actions\/header-actions.module#HeaderActionsModule"
            },
            {
                "path": "header\/profile",
                "loadChildren": ".\/pages\/default\/header\/header-profile\/header-profile.module#HeaderProfileModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule { }