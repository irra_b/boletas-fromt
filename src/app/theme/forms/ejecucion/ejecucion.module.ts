import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EjecucionComponent } from "./ejecucion.component";
import { DefaultComponent } from "../../pages/default/default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../layouts/layout.module";
import {
    DxButtonModule, DxDataGridModule, DxDateBoxModule, DxTabPanelModule, DxTagBoxModule, DxTemplateModule,
    DxTextBoxModule,
    DxTooltipModule,
    DxValidationSummaryModule,
    DxValidatorModule
} from "devextreme-angular";
import { NglModule } from "ng-lightning";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpService } from "../../../_services/http.service";
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": EjecucionComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        DxButtonModule,
        DxDataGridModule,
        DxButtonModule,
        NglModule.forRoot(),
        FormsModule, DxDateBoxModule,
        ReactiveFormsModule,
        DxTextBoxModule,
        DxValidatorModule,
        DxValidationSummaryModule,
        DxTabPanelModule,
        DxTemplateModule,
        DxTagBoxModule,
        DxTooltipModule
    ],
    exports: [RouterModule],
    declarations: [EjecucionComponent],
    providers: [HttpService]
})
export class EjecucionModule { }
