import { Component, OnInit, Attribute, ViewChild } from '@angular/core';
import { AuthService } from "../../../_services/auth.service";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { Location } from "@angular/common";
import { HttpService } from "../../../_services/http.service";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit {

    private title: string;
    private content: string;
    data: any = {
        codigo: '',
        proveedor: '',
        serie: '',
        beneficiario: {},
        concepto: '',
        monto: '',
        banco: {},
        tipo: {},
        fecha_valido: new Date(),
        observaciones: '',
        archivo: []
    };
    alertMessage: string;
    showAlert: boolean = false;
    pick: any;
    showCargando: boolean = false;
    value: any[] = [];
    edit: boolean = false;
    opened: boolean = false;
    form: any;
    type_notif: string;
    showProgress: boolean = false;
    text: string;
    contactos: any = [];
    monedas: any = [{ nombre: 'Bs' }, { nombre: '$us' }];
    multiple: boolean = true;
    @ViewChild('modal') modal;
    now: Date = new Date();
    tipos_contacto: any = [];
    cargos: any[];
    id: number = 0;
    defaultVisible: boolean = false;
    /*---*/
    beneficiarios: any = [];
    tipos: any = [];
    bancos: any = [];
    renov: boolean = false;
    popupVisible: boolean = false;
    entidad: any = { nombre: '' };
    confirmModal: boolean = false;
    titulo: string = '';
    headers: any = {};
    uploadsUrl: string;
    uploadUrl: string;
    showFileMaxMessage: boolean = false;
    @ViewChild('fileUploader') fileUploader: any;
    constructor(active_route: ActivatedRoute,
        private location: Location,
        @Attribute('name') name,
        private _boleta: HttpService,
        private _script: ScriptLoaderService,
        private service: AuthService) {
        this.headers.authorization = this._boleta.getToken();
        this.uploadsUrl = this._boleta.getUploadsUrl().replace('api/boletas/upload/','');
        this.uploadUrl=this._boleta.getUploadsUrl()
        this._boleta.getParams().subscribe(data => {
            this.bancos = data.bancos;
            this.tipos = data.tipos;
            this.beneficiarios = data.beneficiarios;
            this.titulo = 'Nueva boleta de garantía';

            active_route.queryParams.subscribe(params => {
                if (!isNaN(params['id'])) {
                    if (params['renov']) {
                        this.edit = false;
                        this.renov = true;
                        this.titulo = 'Renovación de boleta de garantía';
                    } else {
                        this.renov = false;
                        this.edit = true;
                        this.titulo = 'Modificación de boleta de garantía';
                    }
                    this._boleta.getBoleta(params['id']).subscribe((res: Response) => {
                        //console.log('ENTRO AQUI->', res);
                        this.data = res;
                        let a:any=this.data.archivos;
                        this.data=this.data.boleta;
                        this.data.archivo=a;
                        if (this.data.observaciones == undefined) {
                            this.data.observaciones = '';
                        }
                        if (this.data.serie == undefined) {
                            this.data.serie = '';
                        }
                        this.data.banco = this.bancos.find((item) => {
                            return item.id == this.data.banco.id;
                        });
                        this.data.tipo = this.tipos.find((item) => {
                            return item.id == this.data.tipo.id;
                        });
                        this.data.beneficiario = this.beneficiarios.find((item) => {
                            return item.id == this.data.beneficiario.id;
                        });
                        if (this.data.moneda == 'Bs') {
                            this.data.moneda = this.monedas[0];
                        } else {
                            this.data.moneda = this.monedas[1];
                        }

                    });
                }

            });

        });

    }

    open() {
        this.text = 'Confirme';
        this.opened = !this.opened;
        this.showProgress = false;
    }

    cancel() {
        this.opened = false;
    }
    showPopup() {
        this.popupVisible = true;
    }

    ngOnInit() {
        //console.log('init');

    }
    setTitle(title: string) {
        this.title = title;
    }
    setContent(content: string) {
        this.content = content;
    }


    onSubmit(form: any): void {
        //console.log('onsubmit');
        //console.log(this.data);
        this.form = form;
        this.open();
    }
    sendData() {
        this.text = 'Enviando... ';
        this.showProgress = true;
        this.data.renov = false;
        //console.log(this.data);
        if (this.edit) {
            this._boleta.updateBoleta(this.data).subscribe((res: any) => {
                //console.log('respuesta', res);
                if (res.status == 200) {
                    this.opened = false;
                    this.edit = false;
                    this.form.reset();
                    this.volver();
                } else {
                    this.type_notif = 'error';
                    this.alertMessage = res.msg;
                    this.showAlert = true;
                }
                this.showProgress = false;
            });
        } else {
            if (this.renov) {
                this.data.renov = true;
            } else {
                this.data.renov = false;
            }
            this._boleta.addBoleta(this.data).subscribe((res) => {
                if (res.status == 200) {
                    this.type_notif = 'success';
                    this.opened = false;
                    this.edit = false;
                    this.form.reset();
                    this.volver();
                } else {
                    this.type_notif = 'error';
                    this.alertMessage = res.msg;
                    this.showAlert = true;
                }
                this.showProgress = false;
            });
        }
    }
    showContactosModal() {
        this.modal.openModal(this.cargos);
    }
    onEnd(e) {
        //console.log(e);
        this.data.contactos.push(e);
    }
    remove(i) {
        this.data.archivo.splice(i, 1);
    }
    removeAdj(index) {
        this.data.archivo.splice(index, 1);
    }
    showFile(file) {
        //console.log(file);
        window.open(this.uploadsUrl + file.uri);
    }

    volver() {
        this.location.back();
    }
    onClose(reason: string) {
        //console.log(`Alert closed by ${reason}`);
        this.showAlert = false;
    }
    test(e) {
        //console.log(e);
    }
    addEntidad(f) {
        //console.log(this.entidad);
        this.text = 'Enviando... ';
        this.showProgress = true;
        this._boleta.addEntidad(this.entidad).subscribe((res: Response) => {
            if (res.status == 200) {
                this.type_notif = 'success';
                this.alertMessage = 'Se creo la nueva entidad';
                this.showAlert = true;
                this.popupVisible = false;
                this.getParams();
            } else {
                this.type_notif = 'error';
                this.alertMessage = 'error';
                this.showAlert = true;
            }
            this.showProgress = false;
        });
    }
    getParams() {
        this._boleta.getParams().subscribe(data => {
            this.bancos = data.bancos;
            this.data.banco = this.bancos[this.bancos.length - 1];
        });
    }
    showConfirmModal() {
        this.confirmModal = true;
        this.showProgress = true;
        //console.log(this.data.banco);
        this._boleta.isFree(this.data.banco.id).subscribe(data => {
            if (data[0].total == 0) {
                this.showProgress = false;
                this.text = "Confirme la eliminación de la entidad " + this.data.banco.nombre;
            } else {
                this.type_notif = 'error';
                this.alertMessage = 'La entidad seleccionada no puede ser eliminada por que tiene boletas asociadas a el.';
                this.showAlert = true;
                this.confirmModal = false;
                this.showProgress = false;
            }

        });

    }
    deleteEntidad() {
        this.showProgress = true;
        this.text = "Eliminando...";
        this._boleta.deleteEntidad(this.data.banco.id).subscribe(data => {
            if (data.status == 200) {
                //console.log(this.data.banco);
                this.showProgress = false;
                this.confirmModal = false;
                this.getParams();
                this.data.banco = this.bancos[0];
                //console.log(this.data.banco);

            } else {
                this.type_notif = 'error';
                this.alertMessage = data.msg;
                this.showAlert = true;
            }

        });
    }
    progress(e) {
        //console.log('PROGRESS', e);
    }

    upload(f, start, end) {
        var reader = new FileReader();

        // If we use onloadend, we need to check the readyState.
        reader.onloadend = (evt) => {
            //console.log('DONE', reader.result);

        };
        var blob = f.slice(start, end);
        reader.readAsBinaryString(blob);
    }
    uploaded(e) {
        //console.log('END', JSON.parse(e.request.response));
        this.data.archivo.push(JSON.parse(e.request.response));
        //console.log(this.data);
    }
    uploadError(e) {
        //console.log('ERR', e);
    }
    changed(e) {
        //console.log('CHN', e.value[0].size);
        if (e.value[0].size > 20971520) {
            this.fileUploader.instance.reset();
            this.showFileMaxMessage = true;
        } else {
            this.showFileMaxMessage = false;
        }

    }


}
