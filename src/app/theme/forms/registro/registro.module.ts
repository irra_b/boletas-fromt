import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroComponent } from "./registro.component";
import { RouterModule, Routes } from "@angular/router";
import { NglModule } from "ng-lightning";
import {
    DxButtonModule, DxTemplateModule, DxDataGridModule, DxDateBoxModule, DxTextBoxModule,
    DxValidationSummaryModule, DxValidatorModule, DxTabPanelModule, DxDropDownBoxModule, DxTagBoxModule,
    DxTooltipModule, DxPopupModule, DxTextAreaModule, DxFormModule, DxFileUploaderModule
} from "devextreme-angular";
import { LayoutModule } from "../../layouts/layout.module";
import { DefaultComponent } from "../../pages/default/default.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ContactoComponent } from "../../components/contacto/contacto.component";
import { HttpService } from "../../../_services/http.service";
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RegistroComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        DxButtonModule,
        DxDataGridModule,
        DxButtonModule,
        NglModule.forRoot(),
        FormsModule, DxDateBoxModule,
        ReactiveFormsModule,
        DxTextBoxModule,
        DxValidatorModule,
        DxValidationSummaryModule,
        DxTabPanelModule,
        DxTemplateModule,
        DxTagBoxModule,
        DxTooltipModule,
        DxPopupModule,
        DxTextAreaModule,
        DxFormModule,
        DxFileUploaderModule


    ], exports: [
        RouterModule
    ], declarations: [
        RegistroComponent, ContactoComponent
    ], providers: [HttpService]
})
export class RegistroModule { }
