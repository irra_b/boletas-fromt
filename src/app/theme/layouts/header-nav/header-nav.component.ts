import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { AuthService } from "../../../_services/auth.service";
import { Subscription } from "rxjs/Subscription";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import {noUndefined} from "@angular/compiler/src/util";

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    user: any = {};
    private notif: Subscription = null;
    private profile: Subscription = null;
    data: any = [];
    constructor(private service: AuthService, private notifService: ScriptLoaderService) {
    }
    ngOnInit() {
        this.user=this.service.getUserData();
        this.notif = this.notifService.notif$.subscribe((p) => {
            this.data = p;
        });
        this.profile=this.notifService.profile$.subscribe((r)=>{
            this.user=r[0];
            console.log('__>',this.user);
            if(this.user.img==undefined){
                this.user.img='./assets/images/avatar1.jpg';
            }
        });
    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }

}