import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-contacto',
    templateUrl: './contacto.component.html',
    styles: []
})
export class ContactoComponent implements OnInit {
    openedModal: boolean = false;
    contacto: any = { nombre: '', cargo: [], telefono: '', email: '', cargo_selected: {} };
    v: any = { nombre: false, telefono: false, email: true, cargo_selected: false, veriry: false };
    @Output() onEnd = new EventEmitter<any>();
    constructor() {

    }

    ngOnInit() {

    }
    verify() {
        this.v.veriry = this.v.nombre && this.v.telefono && this.v.email && this.v.cargo_selected;
    }
    verifyText(d) {
        var patt = new RegExp("^[a-zA-Z áéíóúÁÉÍÓÚÑñ]{3,50}$");
        this.v.nombre = patt.test(d);
        this.verify();
    }
    verifyCargo(d) {
        this.v.cargo_selected = true;
        this.verify();
    }
    verifyTelefono(d) {
        var patt = new RegExp("^[0-9]{6,15}$");
        this.v.telefono = patt.test(d);
        this.verify();
    }
    verifyEmail(d) {
        if (d != '') {
            var patt = new RegExp("^[a-zA-Z0-9\-\_\.]{3,50}@[a-zA-Z0-9\-\_\.]{3,50}\.[a-zA-Z0-9]{2,10}$");
            this.v.email = patt.test(d);
        } else {
            this.v.email = true;
        }
        this.verify();

    }
    openModal(e) {
        this.contacto.cargo = e;
        this.openedModal = !this.openedModal;
    }

    cancelModal() {
        this.openedModal = false;
        this.v = { nombre: false, telefono: false, email: false, cargo_selected: false, veriry: false };
        this.contacto = { nombre: '', cargo: [], telefono: '', email: '', cargo_selected: {} };
    }
    add() {
        //this.contacto.cargo=this.contacto.cargo.nombre;
        this.onEnd.emit(this.contacto);
        this.openedModal = !this.openedModal;
        this.v = { nombre: false, telefono: false, email: false, cargo_selected: false, veriry: false };
        this.contacto = { nombre: '', cargo: [], telefono: '', email: '', cargo_selected: {} };

    }
}
