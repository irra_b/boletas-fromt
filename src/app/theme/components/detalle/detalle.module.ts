import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleComponent } from "./detalle.component";
import { DefaultComponent } from "../../pages/default/default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../layouts/layout.module";
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DetalleComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
    ],
    declarations: [DetalleComponent],
    exports: [
        RouterModule
    ]
})
export class DetalleModule { }
