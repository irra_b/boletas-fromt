import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { AuthService } from "../../../_services/auth.service";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './detalle.component.html',
    styles: []
})
export class DetalleComponent implements OnInit {
    id: number = 0;
    empresa: any = {};
    rubro: string;
    rubros: any = [];
    tipos: any = [];
    enlaces: any = [];
    departamentos: any = [];
    tipos_contacto: any;
    estados: any = [];
    cargos: any = [];
    convenio: any = {};
    url: string;
    constructor(private location: Location, active_route: ActivatedRoute, private http: Http, private service: AuthService) {
        var headers = new Headers();
        headers.append('authorization', this.service.getUserToken());
        active_route.params.subscribe(params => {
            if (!isNaN(params['id'])) {
                this.id = +params['id']; // (+) converts string 'id' to a number
                this.http.get(this.service.getUrl() + 'empresa/detalle/' + this.id, { headers }).subscribe((res: Response) => {
                    this.empresa = res.json();
                    if (this.empresa.convenio.length > 0) {
                        this.convenio = { responsable: this.empresa.convenio[0].responsable, fecha: this.empresa.convenio[0].fecha };
                    }
                    this.url = service.getUrlUploads();
                    console.log(this.empresa);
                });
            }

        });
    }
    openLink(uri) {
        window.open(this.url + uri, '_blank');

    }

    ngOnInit() {
    }

}
